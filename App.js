import * as React from 'react';
import { Text, View, StyleSheet, TextInput, TouchableHighlight, Image, Dimensions, ScrollView, TouchableOpacity, Animated, Platform, Component, UIManager, FlatList } from 'react-native';
import CheckBox from 'react-native-check-box'
import SvgUri from 'react-native-svg'

import DateTimePicker from "react-native-modal-datetime-picker"

import Constants from 'expo-constants';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

let width = Dimensions.get('window').width
let height = Dimensions.get('window').height

var counter = 0

export default class App extends React.Component {

  state = {
    title: '',
    checklistMap : new Map(),
    checked: false,
    timePickerVisible: false,
    elementIdx: -1
  }

  changeTitle = (title) => {
    this.setState({
      title: title
    })
  }

  constants = {
    defaultTitle: 'Enter title here',
    newItemPlaceholder: 'Enter item name',
    newItemDescriptionPlaceholder: 'Enter description here'
  }

  showDateTimePicker = (index) => {
    this.setState({ 
      timePickerVisible: true,
      elementIdx: index
    });
  };

  hideDateTimePicker = () => {
    this.setState({ timePickerVisible: false });
  };

  handleDatePicked = date => {
    this.hideDateTimePicker();

    var x = this.state.checklistMap.get(this.state.elementIdx)
    var y = (date.getHours() > 12 ? date.getHours() - 12 : date.getHours()) + ":"
    
    var minutes = date.getMinutes().toString();

    while(minutes.length < 2)
      minutes = '0' + minutes
    y += minutes;

    if(date.getHours() > 12)
      y += " PM"

    x.time = y

    //this.state.checklistMap.set(this.state.elementIdx, x)

    this.setState({
      checklistMap: this.state.checklistMap
    })
  };

  usedIds = new Set()

  upperRandomLimit = 10000

  generateUnusedRandom = () => {
    var current = Math.floor(Math.random() * this.upperRandomLimit)
    while(this.usedIds.has(current)){
      current = Math.floor(Math.random() * this.upperRandomLimit)
    }
    this.usedIds.add(current)

    return current;
  }

  check = (id) => {
    var newChecklistMap = new Map();

    for (var [key, value] of this.state.checklistMap) {
      newChecklistMap.set(key, value)
    }
    var updated = newChecklistMap.get(id);
    updated.checked = !updated.checked;
    newChecklistMap.set(id, updated)

    this.setState({
      checklistMap: newChecklistMap
    })

    this.forceUpdate()
  }

  addItem = () => {
    var newIdx = this.generateUnusedRandom();
    
    var newChecklistMap = new Map();

    for (var [key, value] of this.state.checklistMap) {
      newChecklistMap.set(key, value)
    }

    newChecklistMap.set(newIdx, {
      title: '',
      description: '',
      checked: false,
      id: newIdx,
      time: "Set time"
    })

    this.setState({
      checklistMap: newChecklistMap
    })
  }

  changeText(id, text){
    this.state.checklistMap.get(id).description = text
    this.setState({
      checklistMap: this.state.checklistMap
    })
  }

  changeItemTitle(id, text){
    this.state.checklistMap.get(id).title = text
    this.setState({
      checklistMap: this.state.checklistMap
    })
  }

  deleteItem(id) {
    this.state.checklistMap.delete(id);
    this.usedIds.delete(id)

    this.setState({
      checklistMap: this.state.checklistMap
    })
  }

  render() {
    var arr = [];

    Array.from(this.state.checklistMap, ([key, value]) => {
      arr.push({
        key: key,
        value: value
      })
    })

    return (
      <View style={styles.container}>
        <View style = {styles.nav}>
          <TextInput 
          style={styles.title}
          placeholder = {this.constants.defaultTitle}
          value={this.state.title}
          onChangeText = {() => {this.changeTitle(this.value)}}
          multiline/>
        </View>
        <View style={styles.content}>
          <FlatList
            data={arr}
            extraData={this.state}
            renderItem={({item: thing}) => {
            const key = thing.key
            const value = thing.value
              return <View style={styles.checklistItem}>
                <View style={styles.titleAndDescription}>
                  <TextInput
                    style={styles.itemTitle}
                    placeholder = {this.constants.newItemPlaceholder}
                    value = {value.title}
                    onChangeText={(text) => {this.changeItemTitle(value.id, text)}}>
                  </TextInput>
                  <TextInput
                    style = {styles.itemDescription}
                    placeholder = {this.constants.newItemDescriptionPlaceholder}
                    autogrow = {true}
                    multiline = {true}
                    maxHeight = '50%'
                    numerOfLines = {3}
                    value = {value.description}
                    onChangeText={(text) => {this.changeText(value.id, text)}}>
                  </TextInput>
                </View>
                <View style={styles.checkboxAndTime}>
                  <CheckBox 
                    isChecked = {value.checked} 
                    onClick={() => {this.check(value.id)}} />
                  <TouchableHighlight
                    onPress={() => {this.deleteItem(value.id)}}>
                      <Image
                        style={{
                          width: width / 15,
                          height: width / 15
                        }}
                        source={{uri: 'https://icons-for-free.com/iconfiles/png/512/delete+24px-131985190578721347.png'}}/>
                  </TouchableHighlight>
                  <TouchableHighlight
                    onPress={() => {this.showDateTimePicker(value.id)}}
                    >
                    <Text>
                      {value.time}
                    </Text>
                  </TouchableHighlight>
                </View>
              </View>
            }} />
          <TouchableOpacity
            onPress={this.addItem}
            style={styles.addButton}
            >
              <View
                source={require('./assets/plussign.png')}
                style={styles.addItem}
              >
              </View>
          </TouchableOpacity>
        </View>
        <DateTimePicker
          isVisible={this.state.timePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
          mode={"time"}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  itemTitle: {
    marginLeft: '5%'
  },
  itemDescription: {
    margin: '5%'
  },
  titleAndDescription: {
    flex: 5
  },
  checkboxAndTime: {
    flex: 1
  },
  checklistItem: {
    backgroundColor: 'rgb(100,100,100)',
    height: 0.18 * height,
    width: 0.9 * width,
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    padding: '2.5%',
    margin: '2.5%',
    borderRadius: 10,
    elevation: 5
  },
  addButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width / 7,
    height: width / 7,
    borderRadius: width / 3.5,
    bottom: 5,
    position: 'absolute',
    underlayColor: 'lightBlue',
  },
  /*items: {
    width: '100%',
    height: 5 / 6 * height,
    flex: 5,
    alignItems: 'center',
    padding: '5%'
  },*/
  addItem: {
    width: width / 7,
    height: width / 7,
    borderRadius: width / 3.5,
    bottom: '15%',
    margin: height / 25,
    backgroundColor: '#add8e6'
  },
  title: {
    fontSize: height / 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  nav: {
    flex: 1,
    backgroundColor: 'rgb(100,100,100)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  content: {
    flex: 5,
    backgroundColor: 'rgb(150, 150, 150)',
    width: '100%',
    height: 5 / 6 * height,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
